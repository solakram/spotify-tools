<?php
ini_set('display_errors', 1);
error_reporting(E_ERROR);
session_start();
require_once dirname(__FILE__).'/../vendor/autoload.php';

use \SpotifyTools;
use SpotifyTools\Models\Log;
use SpotifyTools\Utilities\LogUtility;

try {
	\SpotifyTools\Utilities\DatabaseUtility::connect();
	$authService = new SpotifyTools\Services\AuthService();
	$api = $authService->handleWebAuthentication();
	$requestHandler = new SpotifyTools\Handlers\RequestHandler();
	$requestHandler->handleRequest($api);
} catch (\Exception $e) {
	LogUtility::log(new Log(Log::ERROR, null, ["error" => $e]));
	throw $e;
} finally {
	SpotifyTools\Utilities\DatabaseUtility::closeConnection();
}

