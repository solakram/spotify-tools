<?php

namespace SpotifyTools\Utilities;


use SpotifyTools\Services\SpotifyWebApi;

class SortingUtility
{

	public static function sortArtistsByName($artists) {
		usort($artists, function($a, $b) {
			return $a->name > $b->name;
		});

		return $artists;
	}
}