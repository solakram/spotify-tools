<?php

namespace SpotifyTools\Utilities;

use SpotifyTools\Utilities\CurlUtility;
use SpotifyTools\Utilities\UserUtility;

class PlaylistsUtility {

	public static function filterPlaylistsByOwner($playlists, $owner) {
		foreach($playlists->items as $key => $playlist) {
			if($playlist->owner->id != $owner->id) {
				unset($playlists->items[$key]);
			}
		}

		return $playlists;
	}

}