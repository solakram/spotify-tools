<?php

namespace SpotifyTools\Actions\TopArtistsAndTracks;

use SpotifyTools\Actions\AbstractAction;
use SpotifyTools\Utilities\DebugUtility;
use SpotifyTools\Utilities\SortingUtility;
use SpotifyTools\Utilities\TemplateUtility;
use SpotifyWebAPI\SpotifyWebAPIException;

class SaveTopTracksToPlaylist extends AbstractAction {

	const ACTION_NAME = 'saveTopTracksToPlaylist';

	public function execute() {
		if (!array_key_exists('time_range', $_REQUEST)
			|| !array_key_exists('playlist_name', $_REQUEST)
		) {
			throw new \Exception('invalid parameters for '.self::ACTION_NAME);
		}
		$timeRange = $_REQUEST['time_range'];
		if (!array_key_exists($timeRange, TopArtistsAndTracks::TIME_RANGES)) {
			throw new \Exception('Invalid time range');
		}
		$tracks = $this->api->getTopTracks($timeRange);
		$trackIdsToAdd = [];
		foreach($tracks->items as $track) {
			$trackIdsToAdd[] = $track->id;
		}
		$newPlaylistName = $_REQUEST['playlist_name'];
		$newPlaylistResponse = $this->api->createPlaylist(['name' => $newPlaylistName, 'public' => false]);
		$this->api->addAllPlaylistTracks($newPlaylistResponse->id, $trackIdsToAdd);
		echo TemplateUtility::getHtml('TopArtistsAndTracks/SaveTopTracksToPlaylist');
	}

}