<?php

namespace SpotifyTools\Actions\AutomaticPlaylistsIntoPlaylistImporter;

use SpotifyTools\Actions\AbstractAction;
use SpotifyTools\Utilities\DatabaseUtility;
use SpotifyTools\Utilities\DebugUtility;
use SpotifyTools\Utilities\TemplateUtility;
use SpotifyTools\Utilities\UserUtility;

class Save extends AbstractAction {

	public const ACTION_NAME = 'AutomaticPlaylistsIntoPlaylistImporter_save';

	public function execute() {
		$playlistId = $_POST['playlist-id'];
		$user = UserUtility::getSpotifyUser($this->api);
		$importPlaylistsFromRequest = [];
		foreach ($_REQUEST['importPlaylists'] as $importPlaylistRequest) {
			if ($importPlaylistRequest['active'] == 1) {
				$importPlaylistsFromRequest[] = $importPlaylistRequest;
			}
		}
		if (!$importPlaylistsFromRequest) {
			$this->deleteImportPlaylists($user, $playlistId);
			$this->deletePlaylistFromDb($user, $playlistId);
		} else {
			$this->deleteImportPlaylistsNotInRequest($user, $playlistId, $importPlaylistsFromRequest);
			$this->insertOrUpdatePlaylist($user, $playlistId, (bool) $_REQUEST['delete-tracks-if-not-in-import-playlists']);
			$this->insertOrUpdateImportPlaylists($user, $playlistId, $importPlaylistsFromRequest);
		}
		echo TemplateUtility::getHtml('AutomaticPlaylistsIntoPlaylistImporter/Save');
	}

	private function insertOrUpdateImportPlaylists($user, $playlistId, $importPlaylistsFromRequest) {
		foreach ($importPlaylistsFromRequest as $importPlaylistFromRequest) {
			$result = DatabaseUtility::executeBindedQuery("
			SELECT * FROM apipi_import_playlists
			WHERE user_id = ? 
			AND playlist_id = ?
			AND import_playlist_id = ?",
				"sss",
				[$user->id, $playlistId, $importPlaylistFromRequest['playlist-id']]);
			if ($result && $result->num_rows == 1) {
				DatabaseUtility::executeBindedQuery("
					UPDATE apipi_import_playlists
					SET 
						user_id = ?,
						playlist_id = ?,
						import_playlist_id = ?,
						monday = ?,
						tuesday = ?,
						wednesday = ?,
						thursday = ?,
						friday = ?,
						saturday = ?,
						sunday = ?,
						import_time = ?
					WHERE user_id = ?
					AND playlist_id = ?
					AND import_playlist_id = ?",
					"sssiiiiiiissss",
					[
						$user->id, $playlistId, $importPlaylistFromRequest['playlist-id'],
						(int) $importPlaylistFromRequest['importDates']["monday"],
						(int) $importPlaylistFromRequest['importDates']["tuesday"],
						(int) $importPlaylistFromRequest['importDates']["wednesday"],
						(int) $importPlaylistFromRequest['importDates']["thursday"],
						(int) $importPlaylistFromRequest['importDates']["friday"],
						(int) $importPlaylistFromRequest['importDates']["saturday"],
						(int) $importPlaylistFromRequest['importDates']["sunday"],
						$importPlaylistFromRequest['importTime'],
						$user->id, $playlistId, $importPlaylistFromRequest['playlist-id']
					]
				);
			} else {
				DatabaseUtility::executeBindedQuery("
					INSERT INTO apipi_import_playlists (
						user_id,
						playlist_id,
						import_playlist_id,
						monday,
						tuesday,
						wednesday,
						thursday,
						friday,
						saturday,
						sunday,
						import_time
					)
					VALUES (?,?,?,?,?,?,?,?,?,?,?)",
					"sssiiiiiiis",
					[
						$user->id, $playlistId, $importPlaylistFromRequest['playlist-id'],
						(int) $importPlaylistFromRequest['importDates']["monday"],
						(int) $importPlaylistFromRequest['importDates']["tuesday"],
						(int) $importPlaylistFromRequest['importDates']["wednesday"],
						(int) $importPlaylistFromRequest['importDates']["thursday"],
						(int) $importPlaylistFromRequest['importDates']["friday"],
						(int) $importPlaylistFromRequest['importDates']["saturday"],
						(int) $importPlaylistFromRequest['importDates']["sunday"],
						$importPlaylistFromRequest['importTime'],
					]
				);
			}
		}
	}

	private function insertOrUpdatePlaylist($user, $playlistId, bool $deleteTracksIfNotInImportPlaylists) {
		$result = DatabaseUtility::executeBindedQuery("
			SELECT * FROM apipi_playlists
			WHERE user_id = ? 
			AND playlist_id = ?",
			"ss",
			[$user->id, $playlistId]);
		if ($result && $result->num_rows == 1) {
			DatabaseUtility::executeBindedQuery("
			UPDATE apipi_playlists
			SET delete_tracks_if_not_in_import_playlists = ?
			WHERE user_id = ? 
			AND playlist_id = ?",
				"iss",
				[(int) $deleteTracksIfNotInImportPlaylists, $user->id, $playlistId]);
		} else {
			DatabaseUtility::executeBindedQuery("
			INSERT INTO apipi_playlists (user_id, playlist_id, delete_tracks_if_not_in_import_playlists)
			VALUES (?, ?, ?)",
				"ssi",
				[$user->id, $playlistId, (int) $deleteTracksIfNotInImportPlaylists]);
		}
	}

	private function deleteImportPlaylistsNotInRequest($user, $playlistId, $importPlaylistsFromRequest) {
		$importPlaylistIds = [];
		foreach ($importPlaylistsFromRequest as $importPlaylistFromRequest) {
			$importPlaylistIds[] = '"'.DatabaseUtility::getConnection()->escape_string($importPlaylistFromRequest['playlist-id']).'"';
		}
		DatabaseUtility::executeBindedQuery("
			DELETE FROM apipi_import_playlists
			WHERE user_id = ? 
			AND playlist_id = ?
			AND import_playlist_id NOT IN (".implode(",", $importPlaylistIds).")",
			"ss",
			[$user->id, $playlistId]);
	}

	private function deleteImportPlaylists($user, $playlistId) {
		DatabaseUtility::executeBindedQuery("
			DELETE FROM apipi_import_playlists
			WHERE user_id = ? 
			AND playlist_id = ?",
			"ss",
			[$user->id, $playlistId]);
	}

	private function deletePlaylistFromDb($user, $playlistId) {
		DatabaseUtility::executeBindedQuery("
			DELETE FROM apipi_playlists
			WHERE user_id = ? 
			AND playlist_id = ?",
			"ss",
			[$user->id, $playlistId]);
	}
}