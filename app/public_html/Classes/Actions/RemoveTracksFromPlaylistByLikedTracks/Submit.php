<?php

namespace SpotifyTools\Actions\RemoveTracksFromPlaylistByLikedTracks;

use SpotifyTools\Actions\AbstractAction;
use SpotifyTools\Utilities\DebugUtility;
use SpotifyTools\Utilities\TemplateUtility;

class Submit extends AbstractAction {

	public const ACTION_NAME = 'RemoveTracksFromPlaylistByLikedTracks_submit';

	public function execute() {
		if (!array_key_exists('playlist', $_REQUEST) || !$_REQUEST['playlist']) {
			throw new \Exception('Not all needed parameters are set. '.self::class);
		}
		$playlistId = $_REQUEST['playlist'];
		$likedTracks = $this->getLikedTracks();
		$tracksFromPlaylist = $this->getTracksByPlaylist($playlistId);
		$tracksToDelete = array_intersect_key($likedTracks, $tracksFromPlaylist);
		if ($tracksToDelete) {
			$this->api->deleteAllPlaylistTracks($playlistId, array_keys($tracksToDelete));
		}
		echo TemplateUtility::getHtml('RemoveTracksFromPlaylistByLikedTracks/Submit', [
			'numOfDeletedTracks' => count($tracksToDelete),
		]);
	}

	private function getTracksByPlaylist($playlistId) {
		$result = $this->api->getAllPlaylistTracks($playlistId, ["fields" => "items(track(id,name)),next"]);
		$tracks = [];
		foreach ($result->items as $item) {
			$tracks[$item->track->id] = $item->track->name;
		}

		return $tracks;
	}

	private function getLikedTracks() {
		$result = $this->api->getAllMySavedTracks();
		$tracks = [];
		foreach ($result->items as $item) {
			$tracks[$item->track->id] = $item->track->name;
		}

		return $tracks;
	}
}