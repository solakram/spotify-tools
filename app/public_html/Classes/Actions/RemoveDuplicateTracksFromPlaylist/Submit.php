<?php

namespace SpotifyTools\Actions\RemoveDuplicateTracksFromPlaylist;

use SpotifyTools\Actions\AbstractAction;
use SpotifyTools\Utilities\DebugUtility;
use SpotifyTools\Utilities\TemplateUtility;

class Submit extends AbstractAction {

	public const ACTION_NAME = 'RemoveDuplicateTracksFromPlaylist_submit';
	public function execute() {
		if (!array_key_exists('playlist', $_REQUEST) || !$_REQUEST['playlist']
		|| !array_key_exists('playlistSnapshotId', $_REQUEST) || !$_REQUEST['playlistSnapshotId']) {
			throw new \Exception('Not all needed parameters are set. '.self::class);
		}
		$playlistId = $_REQUEST['playlist'];
		$playlistSnapshotId = $_REQUEST['playlistSnapshotId'];
		$trackUrisWithPosition = $_REQUEST['tracks'];
		$deleteTracks = [];
		$numOfDeletions = 0;
		if($trackUrisWithPosition) {
			foreach($trackUrisWithPosition as $trackUri => $positions) {
				$deleteTracks[] = [
					"uri" => $trackUri,
					"positions" => array_map(function($position){ return (int) $position; }, $positions)
				];
				$numOfDeletions += count($positions);
			}
		}
		if ($deleteTracks) {
			$this->api->deleteAllPlaylistTracksWithPositions($playlistId, $deleteTracks, $playlistSnapshotId);
		}

		echo TemplateUtility::getHtml('RemoveDuplicateTracksFromPlaylist/Submit', [
			'numOfDeletedTracks' => $numOfDeletions,
		]);
	}

}