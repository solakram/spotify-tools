<?php

namespace SpotifyTools\Actions\RemoveDuplicateTracksFromPlaylist;

use SpotifyTools\Actions\AbstractAction;
use SpotifyTools\Services\DuplicateTracksService;
use SpotifyTools\Utilities\DebugUtility;
use SpotifyTools\Utilities\TemplateUtility;

class Check extends AbstractAction {

	public const ACTION_NAME = 'RemoveDuplicateTracksFromPlaylist_check';

	public function execute() {
		if (!array_key_exists('playlist', $_REQUEST) || !$_REQUEST['playlist']) {
			throw new \Exception('Not all needed parameters are set. '.self::class);
		}
		$playlistId = $_REQUEST['playlist'];
		$playlist = $this->api->getPlaylist($playlistId);
		$duplicateTracksService = new DuplicateTracksService();
		$tracksFromPlaylist = $this->api->getAllPlaylistTracks($playlistId, ['fields' => DuplicateTracksService::SELECT_FIELDS]);
		$packedDuplicates = $duplicateTracksService->packDuplicatesTogether($tracksFromPlaylist->items);
		echo TemplateUtility::getHtml('RemoveDuplicateTracksFromPlaylist/Check', [
			'packedDuplicates' => $packedDuplicates,
			'playlist' => $playlistId,
			'playlistSnapshotId' => $playlist->snapshot_id,
			'action' => Submit::ACTION_NAME,
		]);
	}

	private function getTracksByPlaylist($playlistId) {
		$result = $this->api->getAllPlaylistTracks($playlistId, ["fields" => "items(track(id,name)),next"]);
		$tracks = [];
		foreach ($result->items as $item) {
			$tracks[$item->track->id] = $item->track->name;
		}

		return $tracks;
	}
}