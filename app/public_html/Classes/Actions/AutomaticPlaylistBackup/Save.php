<?php

namespace SpotifyTools\Actions\AutomaticPlaylistBackup;

use SpotifyTools\Actions\AbstractAction;
use SpotifyTools\Utilities\DatabaseUtility;
use SpotifyTools\Utilities\DebugUtility;
use SpotifyTools\Utilities\TemplateUtility;
use SpotifyTools\Utilities\UserUtility;

class Save extends AbstractAction {

	public const ACTION_NAME = 'AutomaticPlaylistBackup_save';

	public function execute() {
		$user = UserUtility::getSpotifyUser($this->api);
		$backupPlaylistsFromRequest = [];
		foreach ($_REQUEST['playlists'] as $backupPlaylistRequest) {
			if ($backupPlaylistRequest['active'] == 1) {
				$backupPlaylistsFromRequest[] = $backupPlaylistRequest;
			}
		}
		if (!$backupPlaylistsFromRequest) {
			$this->deleteBackupPlaylists($user);
		} else {
			$this->deleteBackupPlaylistsNotInRequest($user, $backupPlaylistsFromRequest);
			$this->insertOrUpdateBackupPlaylists($user, $backupPlaylistsFromRequest);
		}
		echo TemplateUtility::getHtml('AutomaticPlaylistBackup/Save');
	}

	private function insertOrUpdateBackupPlaylists($user, $backupPlaylistsFromRequest) {
		foreach ($backupPlaylistsFromRequest as $backupPlaylistFromRequest) {
			$result = DatabaseUtility::executeBindedQuery("
			SELECT * FROM apb_playlists
			WHERE user_id = ? 
			AND playlist_id = ?",
				"ss",
				[$user->id, $backupPlaylistFromRequest['playlist-id']]);
			if ($result && $result->num_rows == 1) {
				DatabaseUtility::executeBindedQuery("
					UPDATE apb_playlists
					SET 
						user_id = ?,
						monday = ?,
						tuesday = ?,
						wednesday = ?,
						thursday = ?,
						friday = ?,
						saturday = ?,
						sunday = ?,
						backup_time = ?,
						backup_name_scheme = ?
					WHERE user_id = ?
					AND playlist_id = ?",
					"siiiiiiissss",
					[
						$user->id,
						(int) $backupPlaylistFromRequest['backupDates']["monday"],
						(int) $backupPlaylistFromRequest['backupDates']["tuesday"],
						(int) $backupPlaylistFromRequest['backupDates']["wednesday"],
						(int) $backupPlaylistFromRequest['backupDates']["thursday"],
						(int) $backupPlaylistFromRequest['backupDates']["friday"],
						(int) $backupPlaylistFromRequest['backupDates']["saturday"],
						(int) $backupPlaylistFromRequest['backupDates']["sunday"],
						$backupPlaylistFromRequest['backupTime'],
						$backupPlaylistFromRequest['backupNameScheme'],
						$user->id,
						$backupPlaylistFromRequest['playlist-id']
					]
				);
			} else {
				DatabaseUtility::executeBindedQuery("
					INSERT INTO apb_playlists (
						user_id,
						playlist_id,
						monday,
						tuesday,
						wednesday,
						thursday,
						friday,
						saturday,
						sunday,
						backup_time,
						backup_name_scheme
					)
					VALUES (?,?,?,?,?,?,?,?,?,?,?)",
					"isiiiiiiiss",
					[
						$user->id,
						$backupPlaylistFromRequest['playlist-id'],
						(int) $backupPlaylistFromRequest['backupDates']["monday"],
						(int) $backupPlaylistFromRequest['backupDates']["tuesday"],
						(int) $backupPlaylistFromRequest['backupDates']["wednesday"],
						(int) $backupPlaylistFromRequest['backupDates']["thursday"],
						(int) $backupPlaylistFromRequest['backupDates']["friday"],
						(int) $backupPlaylistFromRequest['backupDates']["saturday"],
						(int) $backupPlaylistFromRequest['backupDates']["sunday"],
						$backupPlaylistFromRequest['backupTime'],
						$backupPlaylistFromRequest['backupNameScheme'],
					]
				);
			}
		}
	}

	private function deleteBackupPlaylistsNotInRequest($user, $backupPlaylistsFromRequest) {
		$backupPlaylistIds = [];
		foreach ($backupPlaylistsFromRequest as $backupPlaylistFromRequest) {
			$backupPlaylistIds[] = '"'.DatabaseUtility::getConnection()->escape_string($backupPlaylistFromRequest['playlist-id']).'"';
		}
		DatabaseUtility::executeBindedQuery("
			DELETE FROM apb_playlists
			WHERE user_id = ? 
			AND playlist_id NOT IN (".implode(",", $backupPlaylistIds).")",
			"s",
			[$user->id]);
	}

	private function deleteBackupPlaylists($user) {
		DatabaseUtility::executeBindedQuery("
			DELETE FROM apb_playlists
			WHERE user_id = ? ",
			"s",
			[$user->id]);
	}
}