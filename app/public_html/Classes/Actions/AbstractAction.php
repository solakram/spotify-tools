<?php

namespace SpotifyTools\Actions;

use SpotifyTools\Actions\ArtistTracksFromPlaylistToPlaylist;
use SpotifyTools\Actions\AllTracksByArtist;
use SpotifyTools\Services\SpotifyWebApi;

abstract class AbstractAction {

	const ACTION_NAME_TO_CLASS = [
		//Overview
		Overview::ACTION_NAME => Overview::class,
		//ArtistTracksFromPlaylistToPlaylist
		ArtistTracksFromPlaylistToPlaylist\SelectPlaylist::ACTION_NAME => ArtistTracksFromPlaylistToPlaylist\SelectPlaylist::class,
		ArtistTracksFromPlaylistToPlaylist\SelectArtists::ACTION_NAME => ArtistTracksFromPlaylistToPlaylist\SelectArtists::class,
		ArtistTracksFromPlaylistToPlaylist\Submit::ACTION_NAME => ArtistTracksFromPlaylistToPlaylist\Submit::class,
		//AllTracksByArtist
		AllTracksByArtist\SearchArtist::ACTION_NAME => AllTracksByArtist\SearchArtist::class,
		AllTracksByArtist\SelectArtistAndPlaylist::ACTION_NAME => AllTracksByArtist\SelectArtistAndPlaylist::class,
		AllTracksByArtist\Submit::ACTION_NAME => AllTracksByArtist\Submit::class,
		//AutomaticPlaylistsIntoPlaylistImporter
		AutomaticPlaylistsIntoPlaylistImporter\PlaylistsOverview::ACTION_NAME => AutomaticPlaylistsIntoPlaylistImporter\PlaylistsOverview::class,
		AutomaticPlaylistsIntoPlaylistImporter\EditPlaylist::ACTION_NAME => AutomaticPlaylistsIntoPlaylistImporter\EditPlaylist::class,
		AutomaticPlaylistsIntoPlaylistImporter\Save::ACTION_NAME => AutomaticPlaylistsIntoPlaylistImporter\Save::class,
		//RemoveDuplicateTracksFromPlaylist
		RemoveDuplicateTracksFromPlaylist\SelectPlaylist::ACTION_NAME => RemoveDuplicateTracksFromPlaylist\SelectPlaylist::class,
		RemoveDuplicateTracksFromPlaylist\Check::ACTION_NAME => RemoveDuplicateTracksFromPlaylist\Check::class,
		RemoveDuplicateTracksFromPlaylist\Submit::ACTION_NAME => RemoveDuplicateTracksFromPlaylist\Submit::class,
		//RemoveTracksFromPlaylistByLikedTracks
		RemoveTracksFromPlaylistByLikedTracks\SelectPlaylist::ACTION_NAME => RemoveTracksFromPlaylistByLikedTracks\SelectPlaylist::class,
		RemoveTracksFromPlaylistByLikedTracks\Submit::ACTION_NAME => RemoveTracksFromPlaylistByLikedTracks\Submit::class,
		//AutomaticPlaylistBackup
		AutomaticPlaylistBackup\Settings::ACTION_NAME => AutomaticPlaylistBackup\Settings::class,
		AutomaticPlaylistBackup\Save::ACTION_NAME => AutomaticPlaylistBackup\Save::class,
		//TopArtistsAndTracks
		TopArtistsAndTracks\TopArtistsAndTracks::ACTION_TOP_ARTISTS => TopArtistsAndTracks\TopArtistsAndTracks::class,
		TopArtistsAndTracks\TopArtistsAndTracks::ACTION_TOP_TRACKS => TopArtistsAndTracks\TopArtistsAndTracks::class,
		TopArtistsAndTracks\SaveTopTracksToPlaylist::ACTION_NAME => TopArtistsAndTracks\SaveTopTracksToPlaylist::class,
		//DuplicateTracksOfPlaylists
		DuplicateTracksOfPlaylists\Select::ACTION_NAME => DuplicateTracksOfPlaylists\Select::class,
        DuplicateTracksOfPlaylists\Submit::ACTION_NAME => DuplicateTracksOfPlaylists\Submit::class,
        //Track History
        TrackHistory\View::ACTION_NAME => TrackHistory\View::class,
	];

	/**
	 * @var SpotifyWebApi
	 */
	protected $api;

	public function __construct(SpotifyWebApi $api) {
		$this->api = $api;
	}

	abstract public function execute();
}