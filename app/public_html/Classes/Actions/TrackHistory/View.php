<?php
namespace SpotifyTools\Actions\TrackHistory;

use http\Client\Curl\User;
use SpotifyTools\Actions\AbstractAction;
use SpotifyTools\Services\AuthService;
use SpotifyTools\Utilities\DatabaseUtility;
use SpotifyTools\Utilities\DebugUtility;
use SpotifyTools\Utilities\SortingUtility;
use SpotifyTools\Utilities\TemplateUtility;
use SpotifyTools\Utilities\UserUtility;
use SpotifyWebAPI\SpotifyWebAPIException;

class View extends AbstractAction {

	public const ACTION_NAME = 'trackHistory';

	public function execute() {
        $history = $this->getHistory();
		echo TemplateUtility::getHtml('TrackHistory/View', [
			'history' => $history,
		]);
	}

    private function getHistory() {
        $dbUser = UserUtility::getUserFromPHPSession();
        $result = DatabaseUtility::executeBindedQuery("
            SELECT cth.timestamp, t.*
            FROM current_tracks_history cth
            INNER JOIN tracks t ON cth.track_id = t.id
            WHERE cth.user_id = ?
            GROUP BY cth.timestamp
            ORDER BY cth.timestamp DESC
            LIMIT 1000
        ", "s", [$dbUser['user_id']]);

        return array_map(function($a) {
            $a['artists'] = json_decode($a['artists'], true);
            $a['timestamp'] = (int) substr($a['timestamp'], 0, 10)  + 3600;
            return $a;
        }, $result->fetch_all(MYSQLI_ASSOC));
    }
}