<?php

namespace SpotifyTools\Cronjobs\Traits;

use SpotifyTools\Cronjobs\AbstractCronjob;
use SpotifyTools\Services\SpotifyWebApi;
use SpotifyTools\Utilities\AuthService;
use SpotifyTools\Utilities\DatabaseUtility;
use SpotifyTools\Utilities\DebugUtility;
use SpotifyTools\Utilities\UserUtility;

trait PlaylistHandlingTrait {

	protected $tracksByPlaylist = [];
	/**
	 * @var SpotifyWebApi
	 */
	protected $api;

	protected function getTracksByPlaylist($playlistId) {
		if (!array_key_exists($playlistId, $this->tracksByPlaylist)) {
			$result = $this->api->getAllPlaylistTracks($playlistId);
			$tracks = [];
			foreach ($result->items as $item) {
				$tracks[$item->track->id] = $item->track->name;
			}
			$this->tracksByPlaylist[$playlistId] = $tracks;
		}

		return $this->tracksByPlaylist[$playlistId];
	}
}