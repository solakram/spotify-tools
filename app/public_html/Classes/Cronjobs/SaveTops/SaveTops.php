<?php

namespace SpotifyTools\Cronjobs\SaveTops;

use SpotifyTools\Actions\TopArtistsAndTracks\SaveTopTracksToPlaylist;
use SpotifyTools\Actions\TopArtistsAndTracks\TopArtistsAndTracks;
use SpotifyTools\Cronjobs\AbstractCronjob;
use SpotifyTools\Cronjobs\Traits\PlaylistHandlingTrait;
use SpotifyTools\Models\Log;
use SpotifyTools\Services\SpotifyWebApi;
use SpotifyTools\Utilities\AuthService;
use SpotifyTools\Utilities\DatabaseUtility;
use SpotifyTools\Utilities\DebugUtility;
use SpotifyTools\Utilities\LogUtility;
use SpotifyTools\Utilities\TemplateUtility;
use SpotifyTools\Utilities\UserUtility;

class SaveTops extends AbstractCronjob {

	const CRONJOB_NAME = 'saveTops';
    /**
     * @var SpotifyWebApi
     */
    protected $api;

	function execute($args) {
        $users = UserUtility::getAllUsers();
        $kinds = [TopArtistsAndTracks::KIND_ARTISTS, TopArtistsAndTracks::KIND_TRACKS];
        $timeRange = $args[2];
        foreach($users as $user) {
            try {
                $this->api = $this->authService->getSpotifyWebApiForDbUser($user);
                foreach($kinds as $kind) {
                    $tops = $this->getTopsByKindAndTimeRange($kind, $timeRange);
                    $rankCounter = 1;
                    foreach($tops->items as $top) {
                        $this->save($user, $kind, $timeRange, $rankCounter, $top->id);
                        $rankCounter++;
                    }
                }
            } catch (\Exception $e) {
                LogUtility::log(new Log(Log::ERROR,
                    "saveTops", ["user_id" => $user['user_id'],
                        "error_message" => $e->getMessage()]));
            }
        }
	}

    private function getTopsByKindAndTimeRange($kind, $timeRange) {
        if ($kind == TopArtistsAndTracks::KIND_ARTISTS) {
            return $this->api->getTopArtists($timeRange);
        } else if ($kind == TopArtistsAndTracks::KIND_TRACKS) {
            return $this->api->getTopTracks($timeRange);
        }
    }

	private function save($user, $kind, $timeRange, $rank, $id) {
        DatabaseUtility::executeBindedQuery("INSERT INTO tops_history 
			(user_id, kind, time_range, rank, entity_id)
			VALUES (?, ?, ?, ?, ?)",
            "sssis",
            [$user['user_id'], $kind, $timeRange, $rank, $id]);
    }
}