#Setup

- Checkout this repository
- go to app folder
- copy config.json.dist to config.json and set values accordingly to your needs
- import database.sql into your database (will create the needed database structure)